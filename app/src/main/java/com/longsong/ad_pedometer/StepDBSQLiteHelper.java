package com.longsong.ad_pedometer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;


public class StepDBSQLiteHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "StepDB";

    private static final String KEY_WEEK = "week";
    private static final String KEY_STEPS = "steps";
    private static final String TABLE_STEP = "stepTB";
    private static final String[] COLUMNS = {KEY_WEEK,KEY_STEPS};


    public StepDBSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_STEP_TABLE = "CREATE TABLE IF NOT EXISTS stepTB ( " +
                "week INTEGER PRIMARY KEY , " +
                "steps INTEGER)";

        db.execSQL(CREATE_STEP_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS steps");
        this.onCreate(db);
    }

    public void initTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from stepTB",null);
        if (cursor.getCount()==0){
            ContentValues initValues = new ContentValues();
            for (int i = 1; i <=7; i++) {
                initValues.put(KEY_WEEK,i);
                initValues.put(KEY_STEPS,0);
                db.insert(TABLE_STEP,null,initValues);
            }
        }
    }

    public int getSteps(int week){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_STEP,
                                COLUMNS,
                                "week = ?",
                                new String[] { String.valueOf(week) },
                                null,
                                null,
                                null);
        cursor.moveToFirst();
        //Log.d("getSteps("+week+")", String.valueOf(Integer.parseInt(cursor.getString(1))));

        return Integer.parseInt(cursor.getString(1));
    }

    public List<BarEntry> getAllStepData(){
        List<BarEntry> entries = new ArrayList<>();
        String query = "SELECT  * FROM " + TABLE_STEP;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        BarEntry barEntry = null;
        if (cursor.moveToFirst()){
            do {
                barEntry = new BarEntry(Float.parseFloat(cursor.getString(0)),Float.parseFloat(cursor.getString(1)));

                entries.add(barEntry);
            }while (cursor.moveToNext());
        }
       // Log.d("getAllStepData()", entries.toString());
        return entries;
    }

    public String getPayload(){
        String query = "SELECT  * FROM " + TABLE_STEP;
        SQLiteDatabase db = this.getWritableDatabase();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{");
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()){
            do {
                stringBuffer.append("\""+cursor.getString(0)+"\""+":"+"\""+cursor.getString(1)+"\""+",");
            }while (cursor.moveToNext());
        }
        stringBuffer.deleteCharAt(stringBuffer.length()-1);
        stringBuffer.append("}");

        return stringBuffer.toString();
    }

    public int updateSteps(int week,int steps){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_STEPS,steps+this.getSteps(week));
        int i = db.update(TABLE_STEP,values,KEY_WEEK+"= ?",
                new String[]{String.valueOf(week)});
        return i;
    }

    public long getTotalSteps(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select SUM(steps) from stepTB",null);
        cursor.moveToFirst();
        return Long.parseLong(cursor.getString(0));
    }

    public int initWeekSteps(int week){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_STEPS,0);
        int i = db.update(TABLE_STEP,values,KEY_WEEK+"= ?",
                new String[]{String.valueOf(week)});
        return i;
    }
}

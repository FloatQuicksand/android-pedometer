package com.longsong.ad_pedometer;

public interface StepCountListener {
    void countStep();
}

package com.longsong.ad_pedometer;

public interface StepValuePassListener {
    void stepChanged(int steps);
}

package com.longsong.ad_pedometer;

public interface UpdateUiCallBack {
    void updateUi(int stepCount);
}

# 安卓计步器

#### 介绍
安卓作业，安卓计步软件

# Thanks

| Project/People                                               | desc                                                         | other              |
| ------------------------------------------------------------ | :----------------------------------------------------------- | :----------------- |
| [SQLite](https://www.sqlite.org/)                            | a mobile database                                            | 使用了这个数据库   |
| [MQTT Android](https://github.com/eclipse/paho.mqtt.android) | The Paho Android Service is an MQTT client library written in Java for developing applications on Android | 使用它进行mqtt通信 |
| [MPAndroidChart](https://github.com/PhilJay/MPAndroidChart)  | A powerful & easy to use chart library for Android           | 使用了它的柱状图   |
| [BasePedo](https://github.com/xfmax/BasePedo)                | a android pedometer                                          | 参考了部分算法         |
| [DylanStepCount](https://github.com/linglongxin24/DylanStepCount) | a android pedometer                                          | 参考了部分ui           |
| [android 计步器实现原理操作](https://www.jianshu.com/p/ccf8aac0fb9d) | a step detection method  pedometer                           | 使用了该代码       |